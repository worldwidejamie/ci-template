var { src, dest, watch, series, parallel } = require('gulp')
var styles = require('gulp-sass')
var sourcemaps = require('gulp-sourcemaps')
var autoprefixer = require('autoprefixer')
var sassdoc = require('sassdoc')
var postcss = require('gulp-postcss')
var del = require('del')
var browserSync = require('browser-sync').create()
// var reload = browserSync.reload()

var output = 'styles'

function sass() {
	return src(['styles/styles.scss'])
		.pipe(sourcemaps.init())
		.pipe(styles().on('error', styles.logError))
		.pipe(postcss([autoprefixer()]))
		.pipe(sourcemaps.write())
		.pipe(dest(output))
		.pipe(browserSync.stream())
}

function watchChanges() {
	watch(['**/*.scss', 'styles/**/*.scss'], sass)
	watch(['**/*.php'], reload)
}

function styleDocs() {
	return src(['styles/*.scss', 'styles/**/*.scss'])
		.pipe(sassdoc())
		.resume()
}

function clean() {
	return del(output + 'styles.css')
}

function serve(done) {
	browserSync.init({
		proxy: 'localhost:10075',
		open: false,
		snippetOptions: {
			ignorePaths: 'wp-admin'
		}
	})
	done()
}

function reload(done) {
	browserSync.reload()
	done()
}
// gulp.task('reload', function (done) {
// 	browserSync.reload()
// 	done()
// })

exports.default = series(
	clean,
	parallel(sass, styleDocs),
	serve,
	watchChanges
)
